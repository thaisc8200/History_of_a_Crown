﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestAvailable : DialogueCondition
{
    public override void Trigger()
    {
        NPCQuest npc = (graph as DialogueGraph).Trigger.GetComponent<NPCQuest>();
        List<Quest> quests = npc.Quests;
        foreach (var quest in quests)
        {
            if (quest != null)
            {
                if (QuestManager.Instance.QuestAvailable(quest))
                {
                    npc.CurrentQuest = quest;
                    Success = true;
                    break;
                }
                else
                {
                    Success = false;
                }
            }
        }
        
        base.Trigger();
    }
}
