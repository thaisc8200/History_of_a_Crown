﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIStamina : MonoBehaviour
{
    private Image m_Image;

    private void Start()
    {
        m_Image = GetComponent<Image>();
    }

    public void UpdateStaminaUI(float current, float max)
    {
        m_Image.fillAmount = current / max;
    }
}
