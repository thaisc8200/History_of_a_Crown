﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIQuestListHUD : MonoBehaviour
{
    public RectTransform Layout;
    public GameObject QuestPrefab;
    public int MaxQuests;
    
    public void DisplayQuests(List<QuestStatus> quests)
    {
        EraseContent();
        for (int i = 0; i < quests.Count && i < MaxQuests; i++)
        {
            var quest = quests[i];
            if (quest.Progression.CurrentState != QuestProgression.State.Finished)
            {
                GUIQuestHUD slot = Instantiate(QuestPrefab, Layout).GetComponent<GUIQuestHUD>();
                slot.Init(quest);
            }
        }
    }
    
    public void EraseContent()
    {
        foreach (RectTransform child in Layout)
        {
            Destroy(child.gameObject);
        }
        Layout.DetachChildren();
    }
}
