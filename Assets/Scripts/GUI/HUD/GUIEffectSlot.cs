﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIEffectSlot : MonoBehaviour
{
    public Image Image;
    
    [Header("Sprites")] 
    public Sprite Attack;
    public Sprite CriticalChance;
    public Sprite Defense;
    public Sprite Speed;

    public void Init(Effect effect)
    {
        CheckEffect(effect);
    }

    public void CheckEffect(Effect effect)
    {
        if (effect is AttackBuff)
        {
            Image.sprite = Attack;
        } 
        else if (effect is CriticalChanceBuff)
        {
            Image.sprite = CriticalChance;
        } 
        else if (effect is DefenseBuff)
        {
            Image.sprite = Defense;
        } 
        else if (effect is SpeedBuff)
        {
            Image.sprite = Speed;
        }
    }
}
