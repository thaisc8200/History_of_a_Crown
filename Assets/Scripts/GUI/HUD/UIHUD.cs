﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class UIHUD : Singleton<UIHUD>
{
    private CanvasGroup m_CanvasGroup;
    private Canvas m_Canvas;

    public GUIIndicator Indicator;
    public GUICoins CoinsGUI;
    public CanvasGroup Minimap;
    public GUIQuestListHUD Quests;
    public GUIEffects Effects;
    
    protected override void Awake()
    {
        base.Awake();
        
        m_CanvasGroup = GetComponent<CanvasGroup>();
        m_Canvas = GetComponent<Canvas>();
    }

    public GUIIndicator InitIndicator(Character character)
    {
        return Indicator.Init(character);
    }

    public void DisplayEffects(List<Effect> effects)
    {
        Effects.DisplayEffects(effects);
    }
    
    public void DisplayQuests()
    {
        Quests.DisplayQuests(new NonFinishedQuestFilter().Filter(QuestManager.Instance.Quests));
    }

    public void EnableHUD(bool enable)
    {
        m_CanvasGroup.alpha = enable ? 1f : 0f;
    }

    public void EnableMinimap(bool enable)
    {
        Minimap.alpha = enable ? 1f : 0f;
    }

    public void UpdateCoins(int coins = 0)
    {
        CoinsGUI.UpdateCoins();
        CoinsGUI.UpdateDiffCoins(coins);
    }
}
