﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GUIDiary : Singleton<GUIDiary>
{
    [Header("Achievements")] 
    public List<GUIAchievementList> AchievementLists;
    
    [Header("Quests")] 
    public List<GUIQuestList> QuestLists;
    
    [Header("Journal Entries")] 
    public List<GUIJournalEntryList> EntryLists;

    private CanvasGroup m_CanvasGroup;
    
    protected override void Awake()
    {
        base.Awake();

        m_CanvasGroup = GetComponent<CanvasGroup>();
    }

    public void OpenDiary()
    {
        m_CanvasGroup.alpha = 1;
        m_CanvasGroup.interactable = true;
        m_CanvasGroup.blocksRaycasts = true;
        
        DisplayAchievements();
        DisplayQuests();
        DisplayEntries();
    }

    public void CloseDiary()
    {
        EraseContent();
        EventSystem.current.SetSelectedGameObject(null);
        
        m_CanvasGroup.alpha = 0;
        m_CanvasGroup.interactable = false;
        m_CanvasGroup.blocksRaycasts = false;
    }
    
    private void EraseContent()
    {
        EraseQuestContent();
        EraseAchievementContent();
        EraseEntryContent();
    }
    
    //Quests
    public void DisplayQuests()
    {
        foreach (var list in QuestLists)
        {
            list.DisplayContent(QuestManager.Instance.Quests);
        }
    }
    
    private void EraseQuestContent()
    {
        foreach (var list in QuestLists)
        {
            list.EraseContent();
        }
    }

    private void ReloadQuestContent()
    {
        EraseQuestContent();
        DisplayQuests();
    }
    
    public void AbandonQuest(QuestStatus quest)
    {
        QuestManager.Instance.DeleteQuest(quest.Quest.Id);
        ReloadQuestContent();
    }
    
    //Achievements
    public void DisplayAchievements()
    {
        List<AchievementStatus> achieved = new AchievedFilter().Filter(AchievementManager.Instance.Achievements);
        foreach (var list in AchievementLists)
        {
            list.DisplayContent(achieved);
        }
    }

    private void EraseAchievementContent()
    {
        foreach (var list in AchievementLists)
        {
            list.EraseContent();
        }
    }
    
    //Journal Entries
    public void DisplayEntries()
    {
        List<JournalEntryStatus> achieved = new EntryAchievedFilter().Filter(JournalEntryManager.Instance.Entries);
        achieved.Sort();
        foreach (var list in EntryLists)
        {
            list.DisplayContent(achieved);
        }
    }
    
    private void EraseEntryContent()
    {
        foreach (var list in EntryLists)
        {
            list.EraseContent();
        }
    }
}
