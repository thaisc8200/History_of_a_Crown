﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIJournalEntryList : MonoBehaviour
{
    public RectTransform Layout;
    public GameObject EntryPrefab;
    public int Page;
    public int MaxEntriesPerPage = 6;
    
    public void DisplayContent(List<JournalEntryStatus> entries)
    {
        int iterator = 0;
        switch (Page)
        {
            case 1:
                iterator = 0;
                break;
            case 2:
                iterator += 5;
                break;
            case 3:
                iterator += 11;
                break;
            case 4:
                iterator += 16;
                break;
            case 5:
                iterator += 21;
                break;
            case 6:
                iterator += 26;
                break;
            default:
                iterator = 0;
                break;
        }

        int max = iterator + MaxEntriesPerPage;
        for (int i = iterator; i < entries.Count && i < max; i++)
        {
            var achievement = entries[i];
            GUIJournalEntrySlot slot = Instantiate(EntryPrefab, Layout).GetComponent<GUIJournalEntrySlot>();
            slot.Init(achievement);
        }
    }

    public void EraseContent()
    {
        foreach (RectTransform child in Layout)
        {
            Destroy(child.gameObject);
        }
        Layout.DetachChildren();
    }
}
