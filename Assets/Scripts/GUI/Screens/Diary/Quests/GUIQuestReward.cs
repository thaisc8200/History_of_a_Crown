﻿using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIQuestReward : MonoBehaviour
{
    public Image Image;
    public TextMeshProUGUI Amount;

    public void InitItem(ItemReward item)
    {
        Image.sprite = ItemDB.Instance.GetById(item.Item).Icon;
        Amount.text = item.ItemNumber.ToString();
    }
    
    public void SetAmount(int amount)
    {
        Amount.text = amount.ToString();
    }
}
