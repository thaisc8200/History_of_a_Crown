﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIStats : Singleton<GUIStats>
{
    public Image Character;
    public TextMeshProUGUI Gold;

    [Header("Stats")]
    public TextMeshProUGUI Level;
    public TextMeshProUGUI Health;
    public TextMeshProUGUI Stamina;
    public TextMeshProUGUI Attack;
    public TextMeshProUGUI Defense;
    public TextMeshProUGUI Critical;
    
    private NumberFormatInfo m_NumberFormatter;

    protected override void Awake()
    {
        base.Awake();
        
        m_NumberFormatter = new NumberFormatInfo
        {
            NumberGroupSeparator = ","
        };
    }

    public void Init(Transform opener)
    {
        Character character = opener.GetComponent<Character>();

        Character.sprite = (character.Data as CharacterSO).Portrait; 
        Gold.text = MatchManager.Instance.CurrentCoins.ToString("N0", m_NumberFormatter);

        Level.text = MatchManager.Instance.LevelSystem.Level.ToString();
        Health.text = character.CurrentStats.Health.ToString();
        Stamina.text = character.CurrentStats.Stamina.ToString();
        Attack.text = character.CurrentStats.Attack.ToString();
        Defense.text = character.CurrentStats.Defense.ToString();
        Critical.text = (character.CurrentStats.CriticalChance * 100) + "%";
    }
}
