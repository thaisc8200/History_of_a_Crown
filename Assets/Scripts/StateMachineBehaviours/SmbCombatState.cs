﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmbCombatState : StateMachineBehaviour
{
    [Header("Technique")]
    public CombatTechnique Technique;

    protected AgentCombatSystem m_Combat;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        base.OnStateEnter(animator, stateInfo, layerIndex);

        m_Combat = animator.GetComponent<AgentCombatSystem>();

        if (m_Combat != null)
        {
            ActionOnEnter();
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        m_Combat.OnTechniqueEnd();
    }
    
    protected virtual void ActionOnEnter()
    {
        m_Combat.OnTechniqueStart(Technique);
    }
}
