﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchievementStatus
{
   public Achievement Achievement;
   public AchievementProgression Progression;

   public AchievementStatus(Achievement achievement)
   {
      Achievement = achievement;
      Progression = new AchievementProgression(achievement);
   }
   
   public AchievementStatus(Achievement achievement, AchievementProgression progression)
   {
      Achievement = achievement;
      Progression = progression;
   }
   
   public void UpdateKillGoals(int enemyID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is KillAchievementProgressGoal && !goal.Finished && goal.ID == enemyID)
         {
            (goal as KillAchievementProgressGoal).Current++;

            if ((goal as KillAchievementProgressGoal).Required <= (goal as KillAchievementProgressGoal).Current)
            {
               goal.Finished = true;
            }
            else
            {
               goal.Finished = false;
            }

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateDieGoals(int enemyID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is DieAchievementProgressGoal && !goal.Finished && goal.ID == enemyID)
         {
            (goal as DieAchievementProgressGoal).Current++;

            if ((goal as DieAchievementProgressGoal).Required <= (goal as DieAchievementProgressGoal).Current)
            {
               goal.Finished = true;
            }
            else
            {
               goal.Finished = false;
            }

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateStatGoals(int statID, int amount)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is StatAchievementProgressGoal && !goal.Finished && goal.ID == statID)
         {
            (goal as StatAchievementProgressGoal).Current+= amount;

            if ((goal as StatAchievementProgressGoal).Required <= (goal as StatAchievementProgressGoal).Current)
            {
               goal.Finished = true;
            }
            else
            {
               goal.Finished = false;
            }

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateUseItemGoals(int itemID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is UseItemAchievementProgressGoal && !goal.Finished && goal.ID == itemID)
         {
            (goal as UseItemAchievementProgressGoal).Current++;

            if ((goal as UseItemAchievementProgressGoal).Required <= (goal as UseItemAchievementProgressGoal).Current)
            {
               goal.Finished = true;
            }
            else
            {
               goal.Finished = false;
            }

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateNPCGoals(int npcID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is NPCAchievementProgressGoal && !goal.Finished && goal.ID == npcID)
         {
            goal.Finished = true;

            UpdateProgressComplete();
         }
      }
   }
    
   public void UpdateZoneGoals(int zoneID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is ZoneAchievementProgressGoal && !goal.Finished && goal.ID == zoneID)
         {
            goal.Finished = true;

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateInteractGoals(int triggerID)
   {
      foreach (var goal in Progression.Goals)
      {
         if (goal is InteractTriggerAchievementProgressGoal && !goal.Finished && goal.ID == triggerID)
         {
            goal.Finished = true;

            UpdateProgressComplete();
         }
      }
   }
   
   public void UpdateProgressComplete()
   {
      if (Progression.IsFinished)
      {
         return;
      }

      foreach (var goal in Progression.Goals)
      {
         if (!goal.Finished)
         {
            Progression.IsFinished = false;
            return;
         }
      }

      Progression.IsFinished = true;
      EventManager.TriggerEvent(new AchievementEvent(true, Achievement));
   }
}

[Serializable]
public class AchievementProgression
{
   public List<AchievementProgressGoal> Goals;
   
   public bool IsFinished;

   public AchievementProgression(Achievement achievement)
   {
      Goals = new List<AchievementProgressGoal>();

      foreach (KillAchievementGoal goal in achievement.EnemyAmount)
      {
         Goals.Add(new KillAchievementProgressGoal(goal));
      }
      foreach (DieAchievementGoal goal in achievement.DeathAmount)
      {
         Goals.Add(new DieAchievementProgressGoal(goal));
      }
      foreach (StatAchievementGoal goal in achievement.StatAmount)
      {
         Goals.Add(new StatAchievementProgressGoal(goal));
      }
      foreach (UseItemAchievementGoal goal in achievement.ItemAmount)
      {
         Goals.Add(new UseItemAchievementProgressGoal(goal));
      }
      foreach (NPCAchievementGoal goal in achievement.ActivateNpcID)
      {
         Goals.Add(new NPCAchievementProgressGoal(goal));
      }
      foreach (ZoneAchievementGoal goal in achievement.ActivateZoneID)
      {
         Goals.Add(new ZoneAchievementProgressGoal(goal));
      }
      foreach (InteractTriggerAchievementGoal goal in achievement.Interactions)
      {
         Goals.Add(new InteractTriggerAchievementProgressGoal(goal));
      }

      IsFinished = false;
   }
}
