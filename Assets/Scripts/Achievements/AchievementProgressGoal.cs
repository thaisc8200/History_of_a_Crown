﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AchievementProgressGoal
{
    public int ID { get; set; }
    public string Name { get; set; }
    public bool Finished { get; set; }

    public AchievementProgressGoal(int id, string name)
    {
        ID = id;
        Name = name;
    }
}

[Serializable]
public class KillAchievementProgressGoal : AchievementProgressGoal
{
    public int Required;
    public int Current;

    public KillAchievementProgressGoal(int id, string name, int amount) : base(id, name)
    {
        Current = 0;
        Required = amount;
    }

    public KillAchievementProgressGoal(KillAchievementGoal goal) : base(goal.ID, goal.Name)
    {
        Current = 0;
        Required = goal.RequiredAmount;
    }
}

[Serializable]
public class DieAchievementProgressGoal : AchievementProgressGoal
{
    public int Required;
    public int Current;

    public DieAchievementProgressGoal(int id, string name, int amount) : base(id, name)
    {
        Current = 0;
        Required = amount;
    }

    public DieAchievementProgressGoal(DieAchievementGoal goal) : base(goal.ID, goal.Name)
    {
        Current = 0;
        Required = goal.RequiredAmount;
    }
}

[Serializable]
public class StatAchievementProgressGoal : AchievementProgressGoal
{
    public int Required;
    public int Current;

    public StatAchievementProgressGoal(int id, string name, int amount) : base(id, name)
    {
        Current = 0;
        Required = amount;
    }

    public StatAchievementProgressGoal(StatAchievementGoal goal) : base(goal.ID, goal.Name)
    {
        Current = 0;
        Required = goal.RequiredAmount;
    }
}

[Serializable]
public class UseItemAchievementProgressGoal : AchievementProgressGoal
{
    public int Required;
    public int Current;

    public UseItemAchievementProgressGoal(int id, string name, int amount) : base(id, name)
    {
        Current = 0;
        Required = amount;
    }

    public UseItemAchievementProgressGoal(UseItemAchievementGoal goal) : base(goal.ID, goal.Name)
    {
        Current = 0;
        Required = goal.RequiredAmount;
    }
}

[Serializable]
public class NPCAchievementProgressGoal : AchievementProgressGoal
{
    public NPCAchievementProgressGoal(int id, string name) : base(id, name)
    {

    }

    public NPCAchievementProgressGoal(NPCAchievementGoal goal) : base(goal.ID, goal.Name)
    {

    }
}

[Serializable]
public class ZoneAchievementProgressGoal : AchievementProgressGoal
{
    public ZoneAchievementProgressGoal(int id, string name) : base(id, name)
    {

    }

    public ZoneAchievementProgressGoal(ZoneAchievementGoal goal) : base(goal.ID, goal.Name)
    {

    }
}

[Serializable]
public class InteractTriggerAchievementProgressGoal : AchievementProgressGoal
{
    public InteractTriggerAchievementProgressGoal(int id, string name) : base(id, name)
    {

    }

    public InteractTriggerAchievementProgressGoal(InteractTriggerAchievementGoal goal) : base(goal.ID, goal.Name)
    {

    }
}
