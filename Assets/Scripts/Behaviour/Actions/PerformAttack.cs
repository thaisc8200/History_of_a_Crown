using UnityEngine;
using BehaviorDesigner.Runtime;
using BehaviorDesigner.Runtime.Tasks;

public class PerformAttack : Action
{
    public string Parameter;

    private EnemyCombatSystem _CombatSystem;
    private Agent _Agent;
    private Animator _Animator;


    public override void OnAwake()
	{
        _CombatSystem = GetComponent<EnemyCombatSystem>();
        _Agent = GetComponent<Agent>();
        _Animator = GetComponent<Animator>();
    }

    public override void OnStart()
    {
        _CombatSystem.PerformAttack(Parameter);
    }

    public override TaskStatus OnUpdate()
	{
        
		return TaskStatus.Success;
	}
}