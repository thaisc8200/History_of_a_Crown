﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Controller))]
[RequireComponent(typeof(AgentHealth))]
public abstract class Agent : MonoBehaviour, EventListener<StateChangeEvent<AgentStates.MovementStates>>
{
    [Header("Data")] 
    public AgentSO Data;
    
    public bool SendStateChangeEvents = true;
    
    protected Stats m_BaseStats;

    protected Animator m_Animator;
    protected Controller m_Controller;
    protected AgentHealth m_Health;
    protected AgentMovement m_Movement;
    protected AgentEffects m_AgentEffects;
    protected AgentStamina m_AgentStamina;

    protected HurtboxBehaviour m_Hurtbox;
    protected HitboxBehaviour m_Hitbox;
    protected DefenseboxBehaviour m_Defensebox;

    public AgentStates CharacterState { get; protected set; }
    
    public StateMachine<AgentStates.CombatStates> CombatState { get; set; }
    public StateMachine<AgentStates.MovementStates> MovementState { get; set; }
    public StateMachine<AgentStates.AgentConditions> ConditionState { get; set; }
    public StateMachine<AgentStates.AgentMovementConditions> FreedomState { get; set; }
    
    protected float MovementHandleStateTime { get; set; }
    public Stats BaseStats { get { return m_BaseStats; } }

    private Coroutine m_DamagedCoroutine;

    public bool IsDead {
        get {
            return ConditionState.CurrentState == AgentStates.AgentConditions.Dead || m_Health.CurrentHealth <= 0;
        }
    }

    [ReadOnly]
    public Stats CurrentStats;

    public abstract IEnumerator OnDeath(Transform damager);
    public abstract IEnumerator OnDamaged(Transform damager);
    public abstract void UpdateHealthUI();
    public abstract void UpdateStaminaUI();
    protected abstract void UpdateBaseStats();


    protected virtual void Awake()
    {
        CharacterState = new AgentStates();
        
        MovementState = new StateMachine<AgentStates.MovementStates>(gameObject, SendStateChangeEvents);
        ConditionState = new StateMachine<AgentStates.AgentConditions>(gameObject, SendStateChangeEvents);
        CombatState = new StateMachine<AgentStates.CombatStates>(gameObject, SendStateChangeEvents);
        FreedomState = new StateMachine<AgentStates.AgentMovementConditions>(gameObject, SendStateChangeEvents);

        m_Animator = GetComponent<Animator>();
        m_Controller = GetComponent<Controller>();
        m_Health = GetComponent<AgentHealth>();
        m_Movement = GetComponent<AgentMovement>();
        m_AgentEffects = GetComponent<AgentEffects>();
        m_AgentStamina = GetComponent<AgentStamina>();

        m_Hurtbox = GetComponentInChildren<HurtboxBehaviour>();
        m_Hitbox = GetComponentInChildren<HitboxBehaviour>();
        m_Defensebox = GetComponentInChildren<DefenseboxBehaviour>();
    }

    protected virtual void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0) return;

        UpdateAnimatorValues();
        //UpdateMovementTime();
    }

    protected virtual void OnEnable()
    {
        this.EventStartListening<StateChangeEvent<AgentStates.MovementStates>>();
    }

    protected virtual void OnDisable()
    {
        this.EventStopListening<StateChangeEvent<AgentStates.MovementStates>>();
    }

    protected void UpdateMovementTime()
    {
        if (ConditionState.CurrentState == AgentStates.AgentConditions.Normal
            && CombatState.CurrentState == AgentStates.CombatStates.NoCombat)
        {
            MovementHandleStateTime += Time.deltaTime;
        }
        else
        {
            MovementHandleStateTime = 0f;
        }
    }

    public void AgentDamaged(Transform damager)
    {
        if (m_DamagedCoroutine != null)
        {
            StopCoroutine(m_DamagedCoroutine);
        }
        m_DamagedCoroutine = StartCoroutine(OnDamaged(damager));
    }
    
    public void AgentDeath(Transform damager)
    {
        if (m_DamagedCoroutine != null)
        {
            StopCoroutine(m_DamagedCoroutine);
        }
        StartCoroutine(OnDeath(damager));
    }
    
    protected void UpdateAnimatorValues()
    {
        m_Animator.SetBool("Grounded", m_Controller.State.IsGrounded);
        m_Animator.SetBool("Stealth", MovementState.CurrentState == AgentStates.MovementStates.Stealth);
    }

    public void EnterStealth()
    {
        MovementState.ChangeState(AgentStates.MovementStates.Stealth);
    }

    public void ExitStealth()
    {
        MovementState.ChangeState(AgentStates.MovementStates.Idle);
    }
    
    public virtual void PlaySFX(AudioClip clip)
    {
        SoundManager.Instance.PlaySound(clip, transform.position);
    }

    public void OnEvent(StateChangeEvent<AgentStates.MovementStates> eventType)
    {
        if(eventType.Target == gameObject && eventType.TargetStateMachine == MovementState)
        {
            MovementHandleStateTime = 0f;
        }
    }
}

