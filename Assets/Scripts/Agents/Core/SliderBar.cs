﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Slider))]
public class SliderBar : MonoBehaviour
{
    protected Slider m_Slider;

    void Awake()
    {
        m_Slider = GetComponent<Slider>();
    }
    
    public virtual void UpdateBar(float currentValue, float minValue, float maxValue)
    {
        m_Slider.minValue = minValue;
        m_Slider.maxValue = maxValue;
        m_Slider.value = currentValue;
    }
}
