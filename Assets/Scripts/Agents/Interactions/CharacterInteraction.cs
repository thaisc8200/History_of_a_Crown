﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Character))]
[RequireComponent(typeof(Controller))]
[RequireComponent(typeof(PlayerInput))]
[RequireComponent(typeof(CharacterJump))]
public class CharacterInteraction : MonoBehaviour, EventListener<TransitionEvent>, EventListener<InteractionEvent>
{
    public static bool InteractionHappening { get; set; }
    
    private PlayerInput m_PlayerInput;
    private Controller m_Controller;
    private Character m_Character;

    private bool m_SaveJumpValue;
    
    public bool InInteractionZone { get; set; }
    public Interactable ButtonInteractionZone { get; set; }
    public bool Interacting { get; set; }

    private void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        m_Controller = GetComponent<Controller>();
        m_Character = GetComponent<Character>();
    }

    private void OnEnable()
    {
        this.EventStartListening<TransitionEvent>();
    }

    private void OnDisable()
    {
        this.EventStopListening<TransitionEvent>();
    }

    private void Update()
    {
        if (GameManager.Instance.Paused || Time.deltaTime == 0) return;

        if (m_Character.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return;
        }

        if (m_PlayerInput.Interact.GetButtonDown())
        {
            Interact();
        }
    }
    
    public void EnterInteractionZone(Interactable interactable)
    {
        InInteractionZone = true;
        ButtonInteractionZone = interactable;
    }

    public void ExitInteractionZone()
    {
        InInteractionZone = false;
        ButtonInteractionZone = null;
    }
    private void Interact()
    {
        if (!InInteractionZone || ButtonInteractionZone == null)
        {
            return;
        }

        if (m_Character.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return;
        }

        if (m_Character.FreedomState.CurrentState != AgentStates.AgentMovementConditions.Free)
        {
            return;
        }

        if (!m_Controller.State.IsGrounded)
        {
            return;
        }

        ButtonInteractionZone.InteractionRequest(m_Character);
    }
    
    public void OnEvent(TransitionEvent eventType)
    {
        switch (eventType.TransitionType)
        {
            case TransitionEvent.Type.Start:
                ExitInteractionZone();
                break;
            case TransitionEvent.Type.End:
                if (InteractionHappening)
                {
                    InteractionHappening = false;
                }
                break;
        }
    }
    
    public void OnEvent(InteractionEvent eventType)
    {
        switch (eventType.InteractionType)
        {
            case InteractionEvent.Type.Start:
                InteractionHappening = true;
                break;
            case InteractionEvent.Type.End:
                InteractionHappening = false;
                break;
        }
    }
    
}
