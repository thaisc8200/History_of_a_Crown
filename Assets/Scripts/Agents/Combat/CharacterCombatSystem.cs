﻿
public class CharacterCombatSystem : AgentCombatSystem
{
    private const string DEFAULT_ATTACK = "DefaultAttack";
    private const string FAST_ATTACK = "FastAttack";
    private const string SLOW_ATTACK = "SlowAttack";
    private const string BLOCK = "Block";
    private const string DODGE = "Dodge";
    
    protected PlayerInput m_PlayerInput;

    protected override void Awake()
    {
        m_PlayerInput = GetComponent<PlayerInput>();
        
        base.Awake();
    }

    protected void Update()
    {
        HandleInput();
    }

    protected void HandleInput()
    {
        if (!CheckAvailability())
        {
            return;
        }
        
        HandleAttackInput();
        HandleDefenseInput();
    }
    
    protected void HandleAttackInput()
    {
        if (m_PlayerInput.Attack.GetButtonDown())
        {
            if(AttackValidation()) TriggerTechnique(DEFAULT_ATTACK);
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Attack.ActionButton));
        } else if (m_PlayerInput.FastAttack.GetButtonDown())
        {
            if(AttackValidation()) TriggerTechnique(FAST_ATTACK);
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.FastAttack.ActionButton));
        } else if (m_PlayerInput.SlowAttack.GetButtonDown())
        {
            if(AttackValidation()) TriggerTechnique(SLOW_ATTACK);
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.SlowAttack.ActionButton));
        }
    }

    protected void HandleDefenseInput()
    {
        if (m_PlayerInput.Block.GetButtonDown())
        {
            if(DefenseValidation()) TriggerTechnique(BLOCK);
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Block.ActionButton));
        } else if (m_PlayerInput.Dodge.GetButtonDown())
        {
            if(DefenseValidation()) TriggerTechnique(DODGE);
            EventManager.TriggerEvent(new QuestProgressEvent(QuestProgressEvent.EventType.Tutorial, (int)m_PlayerInput.Dodge.ActionButton));
        }
    }

    public void OnAgentDamage(Agent damaged, int damage, DamageTechnique technique)
    {
        
    }

    public void OnAgentKill(Agent damaged, int damage, DamageTechnique technique)
    {
        
    }

    protected bool AttackValidation()
    {
        if (m_Agent.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return false;
        }
        
        if (m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Blocking
            || m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Dodging)
        {
            return false;
        }

        if (m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Attacking)
        {
            return false;
        }

        return true;
    }

    protected bool DefenseValidation()
    {
        if (m_Agent.ConditionState.CurrentState != AgentStates.AgentConditions.Normal)
        {
            return false;
        }

        if (!m_Controller.State.IsGrounded)
        {
            return false;
        }
        
        if (m_Agent.FreedomState.CurrentState != AgentStates.AgentMovementConditions.Free)
        {
            return false;
        }

        if (m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Blocking
            || m_Agent.CombatState.CurrentState == AgentStates.CombatStates.Dodging)
        {
            return false;
        }

        return true;
    }

    public override void OnTechniqueStart(CombatTechnique technique)
    {
        base.OnTechniqueStart(technique);
        
        ResetTriggers();
    }

    protected void ResetTriggers()
    {
        m_Animator.ResetTrigger(DEFAULT_ATTACK);
        m_Animator.ResetTrigger(FAST_ATTACK);
        m_Animator.ResetTrigger(SLOW_ATTACK);
        m_Animator.ResetTrigger(BLOCK);
        m_Animator.ResetTrigger(DODGE);
    }
    
}
