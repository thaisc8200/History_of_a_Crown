﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitTechnique : DamageTechnique
{
    [Header("Defense")]
    public bool DefenseAllowed = true;
    
    public virtual void TechniqueHit(Collider hitbox, Collider hurtbox)
    {
        HurtboxBehaviour hurtboxBehaviour = hurtbox.GetComponentInParent<HurtboxBehaviour>();
        HitboxBehaviour hitboxBehaviour = hitbox.GetComponentInParent<HitboxBehaviour>();

        Agent hurtboxAgent = hurtboxBehaviour.BaseGameObject.GetComponent<Agent>();
        
        if (hurtboxAgent != null)
        {
            OnTechniqueHitAgent(hitboxBehaviour.Agent, hurtboxAgent);
        }
        else
        {
            OnTechniqueHitNonAgent(hitboxBehaviour.Agent, hurtboxBehaviour.BaseGameObject);
        }
    }

    public virtual void OnTechniqueHitAgent(Agent damager, Agent damaged)
    {
        DamageValues damageValue = damager.GetComponent<AgentCombatSystem>().CalculateDamageDone(damaged.gameObject, this);
        AgentHealth damagedHealth = damaged.GetComponent<AgentHealth>();
        DamageProperties damageProperties = new DamageProperties(damageValue, damager.transform, this);
        
        int damage = damagedHealth.TakeDamage(damageProperties);
        if (damage > 0)
        {
            if (damagedHealth.CurrentHealth > 0)
            {
                OnAgentDamaged(damager, damaged, damage);
            }
            else
            {
                OnAgentKill(damager, damaged, damage);
            }
        }
    }

    public virtual void OnTechniqueHitNonAgent(Agent damager, GameObject damaged)
    {
        DamageValues damageValue = damager.GetComponent<AgentCombatSystem>().CalculateDamageDone(damaged.gameObject, this);
        Health damagedHealth = damaged.GetComponent<Health>();
        DamageProperties damageProperties = new DamageProperties(damageValue, damager.transform, this);

        damagedHealth.TakeDamage(damageProperties);
    }
}
