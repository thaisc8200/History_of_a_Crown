﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class DialogueCallback : UnityEvent<Character>
{

}

[RequireComponent(typeof(Interactable))]
public class NPCDialogue : MonoBehaviour, EventListener<DialogueEvent>
{
    public DialogueGraph Dialogue;
    
    [Header("Events")]
    public DialogueCallback OnDialogueStart;
    public DialogueCallback OnDialogueEnd;
    
    private Interactable m_Interactable;
    
    void Awake()
    {
        m_Interactable = GetComponent<Interactable>();
    }
    
    void OnEnable()
    {
        this.EventStartListening<DialogueEvent>();
    }
    
    void OnDisable()
    {
        this.EventStopListening<DialogueEvent>();
    }

    public void StartDialogue(Character character)
    {
        character.FreedomState.ChangeState(AgentStates.AgentMovementConditions.Free);
        GUIManager.Instance.OpenDialogue(Dialogue, character.transform, transform, m_Interactable.NumberOfActivations);
        
        OnDialogueStart.Invoke(character);
    }
    
    public void EndDialogue(Character character)
    {
        OnDialogueEnd.Invoke(character);
    }

    public void OnEvent(DialogueEvent eventType)
    {
        if (eventType.Graph == Dialogue && eventType.Type == DialogueEvent.EventType.End)
        {
            Character character = eventType.Opener.GetComponent<Character>();
            EndDialogue(character);
        }
    }
}
