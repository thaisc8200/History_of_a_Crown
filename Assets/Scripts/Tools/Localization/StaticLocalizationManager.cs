﻿#if UNITY_EDITOR

using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace PetoonsStudio.Tools
{
    public class StaticLocalizationManager
    {
        private static Dictionary<string, Dictionary<string, string>> LocalizedText;
        private const string DEFAULT_LOCALE = "en_EN";

        public static string Translation(string type, string key)
        {
            if (LocalizedText == null)
            {
                LoadTranslations();
            }

            if (LocalizedText.TryGetValue(type, out Dictionary<string, string> translations))
            {
                if (translations == null)
                {
                    return string.Empty;
                }

                if (translations.TryGetValue(key, out string value))
                {
                    return value;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }
        }

        public static void LoadTranslations()
        {
            string filePath = System.IO.Path.Combine(Application.streamingAssetsPath + "/Localization/" + DEFAULT_LOCALE + ".json");
            if (File.Exists(filePath))
            {
                string dataAsJson = File.ReadAllText(filePath);
                LocalizedData loadedData = JsonUtility.FromJson<LocalizedData>(dataAsJson);
                LocalizedText = new Dictionary<string, Dictionary<string, string>>();
                for (int i = 0; i < loadedData.Items.Length; i++)
                {
                    Dictionary<string, string> translations = new Dictionary<string, string>();
                    for (int j = 0; j < loadedData.Items[i].Translations.Length; j++)
                    {
                        translations.Add(loadedData.Items[i].Translations[j].Key, loadedData.Items[i].Translations[j].Value);
                    }

                    LocalizedText.Add(loadedData.Items[i].Type, translations);
                }
            }
        }
    }
}
#endif
