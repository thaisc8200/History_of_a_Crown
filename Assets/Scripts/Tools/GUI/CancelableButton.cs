﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CancelableButton : Button, ICancelHandler, IPointerExitHandler , IPointerEnterHandler
{
    public UnityEvent onCancel;

    public void OnCancel(BaseEventData eventData)
    {
        onCancel.Invoke();
    }
    
    public override void OnPointerExit(PointerEventData eventData)
    {
        base.OnPointerExit(eventData);
        if(EventSystem.current.currentSelectedGameObject != null)
        {
            //MMAnimator.SetAnimatorTriggerIfExists(EventSystem.current.currentSelectedGameObject.GetComponent<Animator>(),"Normal");
        }
        EventSystem.current = null;
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        base.OnPointerEnter(eventData);
        EventSystem.current.SetSelectedGameObject(gameObject);
    }
}
