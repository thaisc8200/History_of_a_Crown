﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameMessageSystem : MonoBehaviour
{
    public class Message
    {
        public Sprite icon;
        public string message;

        public Message(Sprite ic, string msg)
        {
            icon = ic;
            message = msg;
        }
    }
    
    public float ShowTime = 4f;

    public Image Icon;
    public TextMeshProUGUI Text;

    private Queue<Message> m_PendingMessages = new Queue<Message>();
    private Message m_CurrentMessage;
    
    public void AddMessage(Sprite icon, string message)
    {
        Message newMessage = new Message(icon, message);
        m_PendingMessages.Enqueue(newMessage);

        if (m_CurrentMessage == null)
        {
            DisplayNextMessage();
        }
    }
    
    private void DisplayNextMessage()
    {
        if (m_PendingMessages.Count <= 0)
        {
            return;
        }

        StartCoroutine(DisplayMessageCo());
    }
    
    private IEnumerator DisplayMessageCo()
    {
        m_CurrentMessage = m_PendingMessages.Dequeue();
        if (m_CurrentMessage.icon == null)
        {
            Icon.enabled = false;
        }
        else
        {
            Icon.sprite = m_CurrentMessage.icon;
        }
        Text.text = m_CurrentMessage.message;

        yield return new WaitForSeconds(ShowTime);

        m_CurrentMessage = null;

        if (m_PendingMessages.Count <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            DisplayNextMessage();
        }
    }
}
