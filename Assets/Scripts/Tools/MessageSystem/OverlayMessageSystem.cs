﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OverlayMessageSystem : MonoBehaviour, ISubmitHandler
{
    public float Padding;
        public float WaitTime = 1f;

        private RectTransform m_Transform;
        private CanvasGroup m_CanvasGroup;

        public TextMeshProUGUI Text;

        public enum WritingType
        {
            Instant, Writing
        }

        public WritingType Writingtype = WritingType.Writing;

        protected GameObject m_NextSelection;
        
        void Awake()
        {
            m_Transform = GetComponent<RectTransform>();
            m_CanvasGroup = GetComponent<CanvasGroup>();
        }
        
        public void NewMessage(string message, TextAnchor textAnchor, GameObject previousSelected)
        {
            m_NextSelection = previousSelected;

            SetMessagePosition(textAnchor);

            StartCoroutine(Write(message));
        }
        
        private IEnumerator Write(string message)
        {
            if (Writingtype == WritingType.Instant)
            {
                Text.text = message;
            }

            EventSystem.current.SetSelectedGameObject(null);

            yield return new WaitForSecondsRealtime(WaitTime);

            if (Writingtype == WritingType.Writing)
            {
                for (int i = 0; i < message.Length; i++)
                {
                    Text.text += message[i];
                    yield return null;
                }
            }
            
            m_CanvasGroup.interactable = true;
            EventSystem.current.SetSelectedGameObject(gameObject);
        }
        
        private void SetMessagePosition(TextAnchor textAnchor)
        {
            switch (textAnchor)
            {
                case TextAnchor.UpperLeft:
                    AnchorPresets.TopLeft(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(Padding, -Padding);
                    break;
                case TextAnchor.UpperCenter:
                    AnchorPresets.TopMiddle(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(0f, -Padding);
                    break;
                case TextAnchor.UpperRight:
                    AnchorPresets.TopRight(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(-Padding, -Padding);
                    break;
                case TextAnchor.MiddleLeft:
                    AnchorPresets.MiddleLeft(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(Padding, 0f);
                    break;
                case TextAnchor.MiddleCenter:
                    AnchorPresets.Middle(m_Transform);
                    m_Transform.anchoredPosition = Vector2.zero;
                    break;
                case TextAnchor.MiddleRight:
                    AnchorPresets.MiddleRight(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(-Padding, 0f);
                    break;
                case TextAnchor.LowerLeft:
                    AnchorPresets.BottomLeft(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(Padding, Padding);
                    break;
                case TextAnchor.LowerCenter:
                    AnchorPresets.BottomMiddle(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(0f, Padding);
                    break;
                case TextAnchor.LowerRight:
                    AnchorPresets.BottomRight(m_Transform);
                    m_Transform.anchoredPosition = new Vector2(-Padding, Padding);
                    break;
            }
        }
        
        public virtual void OnSubmit(BaseEventData eventData)
        {
            EventSystem.current.SetSelectedGameObject(m_NextSelection);

            Destroy(transform.parent.gameObject);
        }
}
