﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "QuestDB", menuName = "G1/Database/QuestDB")]
public class QuestDB : AbstractDatabase<Quest>
{
    private static QuestDB m_Instance = null;

    public static QuestDB Instance
    {
        get
        {
            if (m_Instance == null)
            {
                LoadDatabase();
            }

            return m_Instance;
        }
    }

    protected override void OnAddObject(Quest t)
    {
#if UNITY_EDITOR
        t.name = "QUEST" + t.Id;
        AssetDatabase.AddObjectToAsset(t, this);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    protected override void OnRemoveObject(Quest t)
    {
#if UNITY_EDITOR
        AssetDatabase.RemoveObjectFromAsset(t);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
#endif
    }

    public static void LoadDatabase()
    {
#if UNITY_EDITOR
        m_Instance = (QuestDB)AssetDatabase.LoadAssetAtPath("Assets/ScriptableObjects/Model/QuestDB.asset", typeof(QuestDB));
#else
            AssetBundle bundle = MyAssetBundle.LoadAssetBundleFromFile("g1/quests");
            m_Instance = bundle.LoadAsset<QuestDB>("QuestDB");
            bundle.Unload(false);
#endif
    }

    public static void ClearDatabase()
    {
        m_Instance = null;
    }
}
